<?php
/*
Plugin Name: Relfstore Redirect
Plugin URI: https://3xweb.site
Description: Redirecionamentos personalizados.
Version: 1.0
Author: Douglas de Araújo
Author URI: https://3web.site
License: Kopimi
*/

// primeiro, se eu estiver na página de admin não vou fazer nada (y)
if (is_admin()) return;

// se eu estiver na frontpage vejo se o usuário atual é vendedor, se for mando ele pra home custom
function custom_relfstor_redirect(){

    if ( is_front_page() && is_user_logged_in() ){
       
        $user = wp_get_current_user();
        if ( in_array( 'seller', (array) $user->roles ) ) {
            header('Location: https://relfstor.com.br/home-custom');
            exit();
        }
    }
}
add_action('wp', 'custom_relfstor_redirect');